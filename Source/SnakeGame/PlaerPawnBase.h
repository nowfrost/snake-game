// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlaerPawnBase.generated.h"
#include "Interact.h"
#include "Food.h"

class UCameraComponent;
class ASnakeBase;
class AFood;

UCLASS()
class SNAKEGAME_API APlaerPawnBase : public APawn, public IInteract
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlaerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;
	UPROPERTY()
		AFood* Foods;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

	void CreateSnakeActor();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	float minY = -4700.f; float maxY = 4700.f;
	float minX = -4700.f; float maxX = 4700.f;

	float SpawnZ = 0.f;

	void AddRandomFood();

};
